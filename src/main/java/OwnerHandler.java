import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class OwnerHandler
{
    private String name;
    private int age;
    private String phonenum;
    private String email;
    private int min = 1;
    private int max = 9999;
    private String ownerID;
    private static Owner owner = new Owner();
    private static PetsShop pet = new PetsShop();
    private ArrayList<Pet> pets = new ArrayList<>();
    public ArrayList<Owner> ownerList = new ArrayList<>();
    private Scanner kb = new Scanner(System.in);
    private Owner retrievedOwner;
    private String line;
    private String oldContent = "";
    private String newContent = "";




    public void writeNewOwnerToTextFile()
    {
        try (FileWriter ownerFile = new FileWriter("registeredOwners.txt", true);)
        {
                ownerFile.write(owner.getName() + "," + owner.getAge() + "," + owner.getPhonenum() + "," + owner.getEmail() + "," + owner.getOwnerID() + "\n");
                oldContent = oldContent + (owner.getName() + "," + owner.getAge() + "," + owner.getPhonenum() + "," + owner.getEmail() + "," + owner.getOwnerID() + "\n");
//                allTXTFile+= owner.getName() + "," + owner.getAge() + "," + owner.getPhonenum() + "," + owner.getEmail() + "," + owner.getOwnerID() + "\n";
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public void editOwner()
    {
        int i = 1;
        String searchID;
        System.out.println("Okay let me find them, whats their Owner ID?");
        while (i == 1)
        {
             searchID = kb.nextLine();

            for (int j = 0; j < ownerList.size(); j++)
            {
                if (ownerList.get(j).getOwnerID().equals(searchID))
                {
                    System.out.println(ownerList.get(j).toString());
//                kb.nextLine();
                    System.out.println("Owner found");
                    System.out.println("Which detail do you want to edit");
                    String choice = kb.nextLine();

                    switch (choice.toLowerCase())
                    {
                        case "name":
                            System.out.println("Please enter a new name");
                            String newName;
                            newName = kb.nextLine();
                            newContent = oldContent.replaceAll(ownerList.get(j).getName(), newName);
                            oldContent = newContent;
                            appendInfo();
                            ownerList.get(j).setName(newName);
                            System.out.println(ownerList.get(j).toString());
                            break;

                        case "age":
                            System.out.println("Please enter a new age");
                            int newAge = kb.nextInt();
                            ownerList.get(j).setAge(newAge);
                            newContent = oldContent.replace(Integer.toString(ownerList.get(j).getAge()),Integer.toString(newAge));
                            oldContent = newContent;
                            appendInfo();
                            System.out.println(ownerList.get(j).toString());
                            break;

                        case "phone":
                            System.out.println("Please enter a new phone num");
                            String newNum;
                            newNum = kb.nextLine();
                            ownerList.get(j).setPhonenum(kb.nextLine());
                            newContent = oldContent.replaceAll(ownerList.get(j).getPhonenum(), newNum);
                            oldContent = newContent;
                            appendInfo();
                            System.out.println(ownerList.get(j).toString());
                            break;

                        case "email":
                            System.out.println("Please enter a new email");
                            String newMail = kb.nextLine();
                            newContent = oldContent.replaceAll(ownerList.get(j).getEmail(), newMail);
                            oldContent = newContent;
                            appendInfo();
                            ownerList.get(j).setEmail(newMail);
                            System.out.println(ownerList.get(j).toString());
                            break;

                        default:
                            editOwner();
                    }

                    i++;
                } else if(j == ownerList.size())
                {
                    System.out.println("Sorry that Owner ID was either incorrect or doesnt exist, please try again");

                }
            }

        }
    }

    public void createOwner()
    {
        System.out.println("Can I take your name?");
        owner.name = kb.nextLine();
        System.out.println("And your phone number");
        owner.phonenum = kb.nextLine();
        System.out.println("And a contact email address please");
        owner.email = kb.nextLine();
        System.out.println("How old are you");
        owner.age = kb.nextInt();
        owner.ownerID = "OID" + owner.getRandOID(min, max);
        System.out.println("Do you have a pet?");
        Owner createdOwner = new Owner(owner.name, owner.age, owner.phonenum, owner.email, owner.ownerID);
        ownerList.add(createdOwner);
        if (kb.nextLine().equalsIgnoreCase("Yes"))
        {
            pet.addPetToSys();
        } else
        {
            System.out.println("Ok no problem");
        }
        writeNewOwnerToTextFile();
    }

    private void appendInfo()
    {
        try (FileWriter ownerFile = new FileWriter("registeredOwners.txt");)
        {
           ownerFile.write(newContent);
        } catch (IOException e)
        {
            e.printStackTrace();
        }

    }

    public void loadPreviousOwners()
    {
        try (Scanner scanner = new Scanner(new BufferedReader(new FileReader("registeredOwners.txt"))))
        {
            if (!scanner.hasNextLine())
            {
                throw new NullPointerException();
            }
            while (scanner.hasNextLine())
            {
                String input = scanner.nextLine();
                String[] ownerInfo = input.split(",");
                int readAge = Integer.parseInt(ownerInfo[1]);
                Owner readOwner = new Owner(ownerInfo[0], readAge, ownerInfo[2], ownerInfo[3],ownerInfo[4]);
//                allTXTFile += ownerInfo[0] + readAge + ownerInfo[2] + ownerInfo[3] + ownerInfo[4];
                ownerList.add(readOwner);

                oldContent += input + System.lineSeparator();



            }
        } catch (IOException e)
        {
            e.printStackTrace();
        }

    }
    public void showAllOwners()
    {
        System.out.println(ownerList.toString());
    }

    public void checkIfOwnerExists()
    {
        if (kb.nextLine().equalsIgnoreCase("Yes"))
        {
            System.out.println("Ok, Do they already own a pet with us? :");
            if (kb.nextLine().equalsIgnoreCase("Yes"))
            {
                int i = 1;
                System.out.println("Okay let me find them, whats their Owner ID?");
                while (i == 1)
                {
                    this.ownerID = kb.nextLine();
                    if (ownerList.contains(owner.ownerID))
                    {
                        this.retrievedOwner = ownerList.get(ownerList.indexOf(this.ownerID));
                        i++;
                    } else
                    {
                        System.out.println("Sorry that Owner ID was either incorrect or doesnt exist, please try again");
                    }
                }
            } else
            {
                System.out.println("No Problem");
            }
        }
    }

}
