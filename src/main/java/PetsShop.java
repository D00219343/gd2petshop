import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Array;
import java.sql.SQLOutput;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class PetsShop
{
    public ArrayList<Pet> petArrayList = new ArrayList<>();
    public Pet pet = new Pet();
    public OwnerHandler owner = new OwnerHandler();
    public DateCompare compare = new DateCompare();
    public Scanner kb = new Scanner(System.in);
    public DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
    public LocalDate localDate = LocalDate.now();
    private Owner retrievedOwner;
    private Gender genderClass;

    private void writeNewPetToTextFile()
    {

        try (FileWriter petFile = new FileWriter("petShop.txt", true);)
        {

                petFile.write(pet.getName() + "," + pet.getColours() + "," + pet.getAge() + "," + pet.getGender() + "," + pet.getPettype() + "," + pet.getDateRegistered() + "," + pet.getPetID() + "," + pet.getOwnerID() + "\n");

        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }
    public void editPet()
    {
        int i = 1;
        String searchID;
        System.out.println("Okay let me find them, whats their Pet ID?");
        while (i == 1)
        {
            searchID = kb.nextLine();

            for (int j = 0; j < petArrayList.size(); j++)
            {
                if (petArrayList.get(j).getOwnerID().equals(searchID))
                {
                    System.out.println(petArrayList.get(j).toString());
//                kb.nextLine();
                    System.out.println("Pet found");
                    System.out.println("Which detail do you want to edit");
                    String choice = kb.nextLine();

                    switch (choice.toLowerCase())
                    {
                        case "name":
                            System.out.println("Please enter a new name");
                            String newName;
                            newName = kb.nextLine();

                            petArrayList.get(j).setName(newName);
                            System.out.println(petArrayList.get(j).toString());
                            break;

                        case "age":
                            System.out.println("Please enter a new age");
                            int newAge = kb.nextInt();
                            petArrayList.get(j).setAge(newAge);
                            System.out.println(petArrayList.get(j).toString());
                            break;

                        case "phone":
                            System.out.println("Please enter a new colour");
                            petArrayList.get(j).setColours(kb.nextLine());
                            System.out.println(petArrayList.get(j).toString());
                            break;

                        case "gender":
                            System.out.println("Please enter a new gender");
                            String newGender = kb.nextLine();
                            if (newGender.charAt(0) == 'M')
                            {
                                petArrayList.get(j).gender = genderClass.MALE;
                            }
                            else
                            {
                                petArrayList.get(j).gender = genderClass.FEMALE;
                            }
                            System.out.println(petArrayList.get(j).toString());
                            break;

                        default:
                            editPet();
                    }

                    i++;
                } else if(j == petArrayList.size())
                {
                    System.out.println("Sorry that Pet ID was either incorrect or doesnt exist, please try again");

                }
            }

        }
    }

    
    public void addPetToSys() throws InputMismatchException
    {
        try
        {
            System.out.println("You want to add a pet to the system?, first were going to need some details");
            System.out.println("Can I have the pets name? : ");
            pet.name = kb.nextLine();
            System.out.println("What colour is the pet? :");
            pet.colours = kb.nextLine();
            System.out.println("And how old is the pet? :");
            pet.age = kb.nextInt();
            System.out.println("Whats the pets gender? :");
            kb.nextLine();
            if(kb.nextLine().charAt(0) == 'M'){
                pet.gender = Gender.MALE;
            }
            else{
                pet.gender = Gender.FEMALE;
            }
            pet.dateRegistered = dtf.format(localDate);
            pet.petID = "PID" + pet.getRandPID(1, 9999);
            System.out.println("Now does pet pet have an owner? :");
            owner.checkIfOwnerExists();
            pet.ownerID = "No Owner";
            System.out.println("What type of animal is the pet? Mammal, Bird or Fish");
            checkForPetType();

            writeNewPetToTextFile();
        } catch (InputMismatchException e)
        {
            System.out.println("Invalid Input Please try again, and enter a valid input");
            addPetToSys();
        }
    }

    public Comparator<Pet> petDateCompare = new Comparator<Pet>()
    {
        @Override
        public int compare(Pet o1, Pet o2)
        {
            int x = 0;
            String split[] = new String[2];
            for(int i = 0; i < split.length; i++)
            {
                split = o1.getDateRegistered().split("/");
                x += Integer.parseInt(split[0]);
            }

            int y = 0;
            String split2[] = new String[2];
            for(int i = 0; i < split2.length; i++)
            {
                split2 = o2.getDateRegistered().split("/");
                y += Integer.parseInt(split2[0]);
            }

            if (x < y)
            {
                return y - x;
            }
            if(x > y)
            {
                return x - y;
            }
            else{
                return 0;
            }
        }
    };

    public void sortPetArray()
    {
        if(petArrayList != null)
        {


            Collections.sort(petArrayList, petDateCompare);
            System.out.println(petArrayList.toString());


        }
    }

    public void getStats()
    {
        double mtally = 0;
        double btally = 0;
        double ftally = 0;

        for(int i = 0; i < petArrayList.size(); i++)
        {
            if(petArrayList.get(i).getPettype().equalsIgnoreCase("Mammal"))
            {
                mtally += 1;
            }
            if(petArrayList.get(i).getPettype().equalsIgnoreCase("Bird"))
            {
                btally += 1;
            }
            if(petArrayList.get(i).getPettype().equalsIgnoreCase("Fish"))
            {
                ftally += 1;
            }



        }
        mtally = mtally / petArrayList.size();
        ftally = ftally / petArrayList.size();
        btally = btally / petArrayList.size();

        System.out.println((mtally *  (100/1)) + "% Mammals" + (btally * (100/1)) + "% Birds" + (ftally * (100/1)) + " % Fish");
    }

    public void setOwnerByID(String petID, String OwnerID)
    {
        for(int j = 0; j < petArrayList.size(); ++j)
        {
            if (petArrayList.get(j).getPetID().equalsIgnoreCase(petID))
            {
                petArrayList.get(j).setOwnerID(OwnerID);
            }

        }
    }

    public Gender parseGender(String s) throws NumberFormatException {
        if(s.toUpperCase().charAt(0) == 'M')
        {
            return genderClass.MALE;
        }
        else
        {
            return genderClass.FEMALE;
        }

    }

    private void checkForPetType()
    {
    String ans = kb.nextLine();
    boolean loop = true;

    while(loop)
    {
    switch(ans.toLowerCase())
    {
        case "mammal":
            pet.pettype = "Mammal";
            System.out.println("Is the pet neutered? ");
            if (kb.nextLine().equalsIgnoreCase("Yes"))
            {
                pet.neutered = true;
            } else
            {
                pet.neutered = false;
            }
            Mammal mammal = new Mammal(pet.name, pet.colours, pet.gender, pet.age, pet.pettype, pet.dateRegistered, pet.petID, pet.ownerID, pet.neutered);
            petArrayList.add(mammal);
            loop = false;
            break;

        case "bird":
            pet.pettype = "Bird";
            System.out.println("Can the bird fly? ");
            if (kb.nextLine().equalsIgnoreCase("Yes"))
            {
                pet.canFly = true;
            }
            System.out.println("Whats the birds wingspan? ");
            pet.wingspan = kb.nextInt();
            Bird bird = new Bird(pet.name, pet.colours, pet.gender, pet.age, pet.dateRegistered, pet.petID, pet.ownerID, pet.pettype, pet.wingspan, pet.canFly);
            petArrayList.add(bird);
            loop = false;
            break;

        case "fish":
            pet.pettype = "Fish";
            System.out.println("What water does pet fish live in? (e.g salt, fresh) ");
            pet.watertype = kb.nextLine();
            Fish fish = new Fish(pet.name, pet.colours, pet.gender, pet.age, pet.dateRegistered, pet.petID,pet.pettype ,pet.ownerID, pet.watertype);
            petArrayList.add(fish);
            loop = false;
            break;

        default:
            System.out.println("Please enter a valid pet type, Mammal, Bird, Fish");
            ans = kb.nextLine();
            break;

        }
    }

}

    public void loadPreviousAnimals()
    {
        try (Scanner scanner = new Scanner(new BufferedReader(new FileReader("petShop.txt"))))
        {
            if (!scanner.hasNextLine())
            {
                throw new NullPointerException();
            }
            while (scanner.hasNextLine())
            {
                String input = scanner.nextLine();
                String[] petInfo = input.split(",");
                int readAge = Integer.parseInt(petInfo[2]);
                Gender gender = parseGender(petInfo[3]);
                Pet readPet = new Pet(petInfo[0], petInfo[1], gender, readAge, petInfo[5], petInfo[6], petInfo[7], petInfo[4]);
                petArrayList.add(readPet);


            }
        } catch (IOException e)
        {
            e.printStackTrace();
        }

    }
    @Override
    public String toString()
    {
//        if(this.neutered){
        return "Pet" +
                "name: '" + pet.getName() + '\n' +
                "colours: '" + pet.getColours() + '\n' +
                "gender: '" + pet.getGender() + '\n' +
                "age: " + pet.getAge() + '\n' +
                "neutered: " + "" + '\n' +
                "petID: '" + pet.getPetID() + '\n' +
                "ownerID: '" + pet.getOwnerID() + '\n' +
                "localDate: " + localDate + '\n';

    }


    public void showAllPets()
    {
        System.out.println(petArrayList.toString());
    }

    public void getAllXPet(String xPet)
    {
        String ans = kb.nextLine();
        boolean loop = true;

        while (loop)
        {
            System.out.println("Which Pet Type?");

                switch (ans)
                {
                    case "mammal":
                        for(int i = 0; i< petArrayList.size(); ++i)
                        {
                            System.out.println(petArrayList.get(petArrayList.indexOf("mammal")).toString());
                        }
                    case "bird":
                        for(int i = 0; i< petArrayList.size(); ++i)
                        {
                            System.out.println(petArrayList.get(petArrayList.indexOf("bird")).toString());
                        }

                    case "fish":
                        for(int i = 0; i< petArrayList.size(); ++i)
                        {
                            System.out.println(petArrayList.get(petArrayList.indexOf("fish")).toString());
                        }

                    default:
                        System.out.println("Please enter a valid pet type, Mammal, Bird, Fish");
                        ans = kb.nextLine();
                        break;

                }
            }

    }


//    @Override
//    public int compare(Pet o1, Pet o2)
//    {
//
//
//
//    }

}

