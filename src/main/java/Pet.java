import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class Pet
{
    public static int min = 1;
    public static int max = 9999;
    public String name;
    public String colours;
    public Gender gender;
    public int age;
    public boolean neutered = false;
    public boolean canFly = false;
    public int wingspan;
    public String watertype;
    public String dateRegistered;
    public String petID;
    public String pettype;
    public String ownerID;
    public Scanner kb = new Scanner(System.in);
    public ArrayList<Pet> pets = new ArrayList<>();
    public ArrayList<Owner> ownerList = new ArrayList<>();
    public Owner retrievedOwner;
    public Owner owner = new Owner();


//    Idea was from a previous project but the code is not my own
//    Borrowed from mkyong.com
    public DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
    public LocalDate localDate = LocalDate.now();


//    public Pet(String name, String colours, Gender gender, int age, String dateRegistered, String petID, String ownerID)
//    {
//        this.name = name;
//        this.colours = colours;
//        this.gender = gender;
//        this.age = age;
//        this.dateRegistered = dateRegistered;
//        this.petID = petID;
//        this.ownerID = ownerID;
//    }

    public String getPettype()
    {
        return pettype;
    }

    public Pet(String name, String colours, Gender gender, int age, String dateRegistered,  String petID, String owner, String pettype)
    {
        this.name = name;
        this.colours = colours;
        this.gender = gender;
        this.age = age;
        this.dateRegistered = dateRegistered;
        this.pettype = pettype;
        this.petID = petID;
        this.ownerID = owner;

    }

    public Pet()
    {

    }
//    public Pet(String name, String colours, String gender, int age, String dateRegistered, String petID)
//    {
//        this.name = name;
//        this.colours = colours;
//        this.gender = gender;
//        this.age = age;
//        this.dateRegistered = dateRegistered;
//        this.petID = petID;
//    }

    public Pet(String ownerID)
    {
        this.name = name;
        this.colours = colours;
        this.gender = gender;
        this.age = age;
        this.dateRegistered = dateRegistered;
        this.petID = petID;
        this.ownerID = ownerID;
    }

    public String getName()
    {
        return name;
    }

    public void showAllPets()
    {
        System.out.println(pets.toString());
    }

    public String getColours()
    {
        return colours;
    }

    public String getGender()
    {
        return gender.name();
    }

    public int getAge()
    {
        return age;
    }

    public String getDateRegistered()
    {
        return dateRegistered;
    }

    public String getPetID()
    {
        return petID;
    }

    public String getOwnerID()
    {
        return ownerID;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setColours(String colours)
    {
        this.colours = colours;
    }

    public void setGender(Gender gender)
    {
        this.gender = gender;
    }

    public void setAge(int age)
    {
        this.age = age;
    }

    public void setNeutered(boolean neutered)
    {
        this.neutered = neutered;
    }

    public void setDateRegistered(String dateRegistered)
    {
        this.dateRegistered = dateRegistered;
    }

    public void setPetID(String petID)
    {
        this.petID = petID;
    }

    public void setOwnerID(String ownerID)
    {
        this.ownerID = ownerID;
    }

    public String getRandPID(int min, int max)
    {
        String formattedNums;
        Random r = new Random();
        int nums = r.nextInt((max - min) + 1) + min;
        formattedNums = String.format("%04d", nums);
        for(int i = 0; i < pets.size(); ++i)
        {
            if(pets.get(i).getPetID().contains(formattedNums))
            {
                getRandPID(1, 9999);
                break;
            }
        }
        return formattedNums;
    }

    @Override
    public String toString()
    {
//        if(this.neutered){
        return "Pet" +
                "name: " + name + ',' + '\n' +
                "colours: " + colours + ',' + '\n' +
                "gender:" + gender + ',' + '\n' +
                "age: " + age + ',' + '\n' +
                "neutered: " + neutered + ',' + '\n' +
                "dateRegistered: " + dateRegistered + ',' + '\n' +
                "petID: " + petID + ',' + '\n' +
                "ownerID: " + ownerID + ',' + '\n' +
                "type: " + pettype + ',' + '\n'
                + "*********************** \n";
    }

//    @Override
//    public int compareTo(Pet o)
//    {
//        int x = 0;
//        String split[] = new String[2];
//        for(int i = 0; i < split.length; i++)
//        {
//            split = o.getDateRegistered().split("/");
//            x += Integer.parseInt(split[0]);
//        }
//
//        int y = 0;
//        String split2[] = new String[2];
//        for(int i = 0; i < split2.length; i++)
//        {
//            split2 = this.getDateRegistered().split("/");
//            y += Integer.parseInt(split2[0]);
//        }
//
//        if (x < y)
//        {
//            return -1;
//        }
//        if(x > y)
//        {
//            return 1;
//        }
//        else{
//            return 0;
//        }
//
//    }
}
