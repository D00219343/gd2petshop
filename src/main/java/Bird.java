public class Bird extends Pet
{
    public int wingspan;
    public boolean canFly;

    public Bird(String name, String colours, Gender gender, int age, String dateRegistered, String petID, String ownerID, String pettype, int wingspan, boolean canFly)
    {
        super(name, colours, gender, age, dateRegistered, petID, ownerID, pettype);
        this.wingspan = wingspan;
        this.canFly = canFly;
    }

}
