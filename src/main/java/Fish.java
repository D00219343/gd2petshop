public class Fish extends Pet
{
    public String waterType;

    public Fish(String name, String colours, Gender gender, int age, String dateRegistered, String petID, String ownerID, String pettype, String waterType)
    {
        super(name, colours, gender, age, dateRegistered, petID, ownerID, pettype);
        this.waterType = waterType;
    }
}
