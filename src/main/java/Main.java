import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Scanner;

public class Main
{
    private static PetsShop pet = new PetsShop();
    private static OwnerHandler owner = new OwnerHandler();
    private static final String ANSI_RED = "\u001B[31m";
    private static final String ANSI_GREEN = "\u001b[32m";
    private static final String ANSI_RESET = "\u001B[0m";
    public static void main(String[] args)
    {

        Scanner kb = new Scanner(System.in);


        try
        {
            pet.loadPreviousAnimals();


            System.out.println(ANSI_GREEN + "Previous Pets Loaded");
        } catch (NullPointerException e)
        {
            System.out.println(ANSI_RED + "No Pets Loaded");
        }
        System.out.println(ANSI_RESET);

        try
        {

            owner.loadPreviousOwners();

            System.out.println(ANSI_GREEN + "Previous Owners Loaded");
        } catch (NullPointerException e)
        {
            System.out.print(ANSI_RED + "No Owners Loaded");
        }
        System.out.println(ANSI_RESET);



        int choice;
        boolean exit = false;
        while(exit == false)
        {
            loadMenu();

            choice = kb.nextInt();
            switch (choice)
            {
                case 1:
                    pet.addPetToSys();
                    break;
                case 2:
                    pet.showAllPets();
                    break;
                case 3:
                    owner.createOwner();
                    break;
                case 4:
                    owner.showAllOwners();
                    break;

                case 5:
                    owner.editOwner();
                    break;

                case 6:
                    pet.sortPetArray();
                    break;

                case 7:
                    kb.nextLine();
                    String pid;
                    String oid;
                    System.out.println("Whats the PetsID?");

                    pid = kb.nextLine();
                    System.out.println("And The OwnersID?");
                    oid = kb.nextLine();
                    pet.setOwnerByID(pid, oid);
                    break;

                case 8:
                    pet.getStats();

                case 9:
                    exit = true;


            }
        }
    }

    public static void loadMenu()
    {
        System.out.println("+----------------------------+");
        System.out.println("|--Welcome to the pets shop--|");
        System.out.println("|1.----ADD-PET-TO-SYSTEM-----|");
        System.out.println("|2.----SHOW-ALL-THE-PETS-----|");
        System.out.println("|3.-------CREATE-OWNER-------|");
        System.out.println("|4.--------SHOW-OWNER--------|");
        System.out.println("|5.--------EDIT-OWNER--------|");
        System.out.println("|6.--------SORT--PETS--------|");
        System.out.println("|7.---------SET-OWNER--------|");
        System.out.println("|8.-----------STATS----------|");
        System.out.println("|9.-----------QUIT-----------|");
        System.out.println("+----------------------------+");
    }



}



