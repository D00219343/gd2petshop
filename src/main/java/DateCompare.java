import java.util.Comparator;

public class DateCompare implements Comparator<Pet>
{

    @Override
    public int compare(Pet o1, Pet o2)
    {
        int x = 0;
        String split[] = new String[2];
        for(int i = 0; i < split.length; i++)
        {
            split = o1.getDateRegistered().split("/");
            x += Integer.parseInt(split[0]);
        }

        int y = 0;
        String split2[] = new String[2];
        for(int i = 0; i < split2.length; i++)
        {
            split2 = o2.getDateRegistered().split("/");
            y += Integer.parseInt(split2[0]);
        }

        if (x < y)
        {
            return y - x;
        }
        if(x > y)
        {
            return x - y;
        }
        else{
            return 0;
        }
    }
}
