
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Owner
{
    public String name;
    public int age;
    public String phonenum;
    public String email;
    private int min = 1;
    private int max = 9999;
    public String ownerID;
    private static Pet petObj = new Pet();
    private static PetsShop pet = new PetsShop();
    private ArrayList<Pet> pets = new ArrayList<>();
    private ArrayList<Owner> ownerList = new ArrayList<>();
    private Scanner kb = new Scanner(System.in);
    private Owner retrievedOwner;


    public Owner(String name, String phonenum, String email, String ownerID, Pet pet)
    {
        this.name = name;
        this.phonenum = phonenum;
        this.email = email;
        this.ownerID = ownerID;
        this.pets = pets;
    }

    public Owner(String name, int age, String phonenum, String email, String ownerID)
    {
        this.name = name;
        this.phonenum = phonenum;
        this.email = email;
        this.ownerID = ownerID;
        this.age = age;
    }

    public Owner()
    {

    }

    public String getRandOID(int min, int max)
    {
        String formattedNums;
        Random r = new Random();
        int nums = r.nextInt((max - min) + 1) + min;
        formattedNums = String.format("%04d", nums);
        for(int i = 0; i < ownerList.size(); i++)
        {
            if (ownerList.get(i).getOwnerID().contains(formattedNums))
            {
                getRandOID(1, 9999);
                break;
            }
        }
        return formattedNums;
    }

//    public void loadPreviousOwners()
//    {
//        try (Scanner scanner = new Scanner(new BufferedReader(new FileReader("registeredOwners.txt"))))
//        {
//            if (!scanner.hasNextLine())
//            {
//                throw new NullPointerException();
//            }
//            while (scanner.hasNextLine())
//            {
//                String input = scanner.nextLine();
//                String[] ownerInfo = input.split(",");
//                int readAge = Integer.parseInt(ownerInfo[1]);
//                Owner readOwner = new Owner(ownerInfo[0], readAge, ownerInfo[2], ownerInfo[3],ownerInfo[4]);
//                ownerList.add(readOwner);
//                System.out.println(readOwner.toString());
//
//
//
//            }
//        } catch (IOException e)
//        {
//            e.printStackTrace();
//        }
//
//    }
//
//    public void createOwner()
//    {
//        System.out.println("Can I take your name?");
//        this.name = kb.nextLine();
//        System.out.println("And your phone number");
//        this.phonenum = kb.nextLine();
//        System.out.println("And a contact email address please");
//        this.email = kb.nextLine();
//        System.out.println("How old are you");
//        this.age = kb.nextInt();
//        this.ownerID = "OID" + getRandOID(min, max);
//        System.out.println("Do you have a pet?");
//        Owner createdOwner = new Owner(this.name, this.age, this.phonenum, this.email, this.ownerID);
//        ownerList.add(createdOwner);
//        if (kb.nextLine().equalsIgnoreCase("Yes"))
//        {
//            pet.addPetToSys();
//        } else
//        {
//            System.out.println("Ok no problem");
//        }
//        writeNewOwnerToTextFile();
//    }
//
//    public void editOwner()
//    {
//        int i = 1;
//        System.out.println("Okay let me find them, whats their Owner ID?");
//        while (i == 1)
//        {
//            String searchID = kb.nextLine();
//
//            for (int j = 0; j < ownerList.size(); j++)
//            {
//                if (ownerList.get(j).getOwnerID().equals(searchID))
//                {
//                System.out.println(ownerList.get(j).toString());
////                kb.nextLine();
//                System.out.println("Owner found");
//                System.out.println("Which detail do you want to edit");
//                String choice = kb.nextLine();
//
//                switch (choice.toLowerCase())
//                {
//                    case "name":
//                        System.out.println("Please enter a new name");
//                        ownerList.get(j).setName(kb.nextLine());
//                        System.out.println(ownerList.get(j).toString());
//                }
//                i++;
//                writeNewOwnerToTextFile();
//            } else
//            {
//                System.out.println("Sorry that Owner ID was either incorrect or doesnt exist, please try again");
//            }
//        }
//        }
//    }
//
//    public void writeNewOwnerToTextFile()
//    {
//        try (FileWriter ownerFile = new FileWriter("registeredOwners.txt");)
//        {
//            for (int i = 0; i < ownerList.size(); ++i)
//            {
//
//                ownerFile.write(getName() + "," + getAge() + "," + getPhonenum() + "," + getEmail() + "," + getOwnerID() + "\n");
//            }
//        } catch (IOException e)
//        {
//            e.printStackTrace();
//        }
//    }
//
//
//
//
//

    public String getName()
    {
        return this.name;
    }

    public String getPhonenum()
    {
        return phonenum;
    }

    public String getEmail()
    {
        return email;
    }

    public int getAge()
    {
        return age;
    }

    public String getOwnerID()
    {
        return ownerID;
    }


    public void setName(String name)
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return "Owner{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", phonenum='" + phonenum + '\'' +
                ", email='" + email + '\'' +
                ", ownerID='" + ownerID + '\'' +
                '}';
    }



    public void setOwnerID(String ownerID)
    {
        this.ownerID = ownerID;
    }

    public void setPhonenum(String phonenum)
    {
        this.phonenum = phonenum;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public void setAge(int age)
    {
        this.age = age;
    }
}
