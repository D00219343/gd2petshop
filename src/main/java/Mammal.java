public class Mammal extends Pet
{
    public boolean neutered;

    public Mammal(String name, String colours, Gender gender, int age, String pettype ,String petID, String ownerID, String dateRegistered, boolean neutered)
    {
        super(name, colours, gender, age, dateRegistered, petID, ownerID, pettype);
        this.neutered = neutered;

    }


}
